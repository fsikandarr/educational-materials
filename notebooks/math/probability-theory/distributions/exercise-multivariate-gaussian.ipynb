{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise - Multivariate Gaussian"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Python Modules](#Python-Modules)\n",
    "  * [Knowledge](#Knowledge)\n",
    "  * [Modules](#Python-Modules)\n",
    "* [Exercises](#Exercises)\n",
    "  * [Exercise - Mean and Covariance Matrix](#Exercise---Mean-and-Covariance-Matrix)\n",
    "  * [Plots](#Plots)\n",
    "  * [Exercise - Joint Normality](#Exercise---Joint-Normality)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Introduction\n",
    "\n",
    "To define a univariate gaussian distribution, we only need the variance and the mean. For multivariate gaussian we need the vectorized mean and the covariance matrix. In this notebook you will implement the functions to calculate both, the vectorized `mean`- and the `covariance_matrix`. \n",
    "\n",
    "In a two dimensional vector space, the multivariate gaussian is called bivariate gaussian, which will be used throughout the whole notebook, so we are still able to visualize our data.\n",
    "\n",
    "In order to detect errors in your own code, execute the notebook cells containing `assert` or `assert_almost_equal`. These statements raise exceptions, as long as the calculated result is not yet correct."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "\n",
    "To complete this exercise notebook, you should possess knowledge about the following topics.\n",
    "- Univariate gaussian\n",
    "- Multivariate gaussian\n",
    "- Variance\n",
    "- Covariance\n",
    "- Covariance matrix\n",
    "\n",
    "The following material can help you to acquire this knowledge:\n",
    "* Gaussian, variance, covariance, covariance matrix:\n",
    " * Chapter 3 of the [Deep Learning Book](http://www.deeplearningbook.org/) [GOO16]\n",
    " * Chapter 1 of the book Pattern Recognition and Machine Learning by Christopher M. Bishop [BIS07]\n",
    "* Univariate gaussian:\n",
    " * [Video](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/more-on-normal-distributions/v/introduction-to-the-normal-distribution) and the following of Khan Academy [KHA18]\n",
    "* Multivariate gaussian:\n",
    " * Video PP 6.1 and following in the playlist [Probability Primer](https://www.youtube.com/watch?v=TC0ZAX3DA88&index=34&list=PL17567A1A3F5DB5E4) of the youtube user *mathematicalmonk* [MAT18]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# External Modules\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.cm as cm\n",
    "from scipy.stats import multivariate_normal as mvn\n",
    "from mpl_toolkits.mplot3d import Axes3D"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%%html\n",
    "# prevents output cells to receive a height limit and a scroll bar\n",
    "<style>\n",
    ".output_wrapper, .output {\n",
    "    height:auto !important;\n",
    "    max-height:1000px;  /* your desired max-height here */\n",
    "}\n",
    ".output_scroll {\n",
    "    box-shadow:none !important;\n",
    "    webkit-box-shadow:none !important;\n",
    "}\n",
    "</style>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "From an experiment we obtain size $N$ random samples ${\\bf x}_1, \\dots, {\\bf x}_p$\n",
    "from a population $f(x)$, where each random sample\n",
    "${\\bf x}=[x_1, \\dots, x_N]$ contains $N$ variables. Each variable $x_1, \\dots, x_N$ is normally distributed in itself and independent from each other (${\\bf x}_p \\perp {\\bf x}_{p-1}$).\n",
    "\n",
    "**Sidenote:**\n",
    "\n",
    "*In most literature, bold variables are used to denote different vectors (examples), whereas non-bold variables are used for the elements of one vector.*\n",
    "\n",
    "*Usually the independence sign has two vertical lines, but unfortunately it is not part of the standard $\\mathrm{L\\!\\!^{{}_{A}} \\!\\!\\!\\!\\!\\;\\; T\\!_{\\displaystyle E} \\! X}$ package.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "N = 1000\n",
    "p = 2\n",
    "X = np.zeros((N,2))\n",
    "\n",
    "mean_x1 = -1\n",
    "mean_x2 = 2\n",
    "std_dev_x1 = np.sqrt(0.6)\n",
    "std_dev_x2 = np.sqrt(2.0)\n",
    "\n",
    "X[:,0] = np.random.normal(size=N, loc=mean_x1, scale=std_dev_x1)\n",
    "X[:,1] = np.random.normal(size=N, loc=mean_x2, scale=std_dev_x2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The following plot visualizes the drawn sample."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(7,7))\n",
    "ax = fig.add_subplot(111)\n",
    "ax.scatter(X[...,0], X[...,1], c='b', marker='x', label=\"data\")\n",
    "Y_lim = -1,5\n",
    "X_lim = -4,2\n",
    "\n",
    "ax.set_xlim(X_lim[0],X_lim[1])\n",
    "ax.set_ylim(Y_lim[0],Y_lim[1])\n",
    "ax.set_xlabel('$x_1$')\n",
    "ax.set_ylabel('$x_2$')\n",
    "plt.legend()\n",
    "plt.draw()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise - Mean and Covariance Matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "Write a vectorized `mean`- and the `covariance_matrix`-function using only the numpy \n",
    "methods/functions/operators: `+`, `-`, `*`, `/`,`np.sum(..)`, `np.dot(..)`, `np.shape(..)`, `X.T`)\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "You don't need all of them (depending on your implementation). You can use your implemented `mean` function inside `covariance_matrix`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Implement this function\n",
    "\n",
    "def mean(X):\n",
    "    \"\"\"\n",
    "    Calculates the vectorized mean over the first axis\n",
    "    \n",
    "    :X: input-matrix.\n",
    "    :X type: numpy ndarray with n dimensions (n >= 1) of type float.\n",
    "    \n",
    "    :return: vectorized mean of X.\n",
    "    .return type: numpy ndarray with n-1 dimensions of type float.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Your implementations should pass this test:\n",
    "\n",
    "mu = mean(X)\n",
    "np.testing.assert_almost_equal(mu, X.mean(axis=0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def covariance_matrix(X):\n",
    "    \"\"\"\n",
    "    Calculates the covariance matrix of X\n",
    "    \n",
    "    :X: input-matrix.\n",
    "    :X type: numpy ndarray with shape (n,m) of type float.\n",
    "    \n",
    "    :return: covariance matrix of X.\n",
    "    .return type: numpy ndarray with shape (m,m) of type float.\n",
    "    \"\"\"\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Your implementations should pass this test:\n",
    "\n",
    "cov = covariance_matrix(X)\n",
    "cov_np = np.cov(X, rowvar=0)\n",
    "np.testing.assert_array_almost_equal(cov, cov_np)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "To examine the underlying distribution of our drawn sample, we could, e.g. plot all the individual dimensions as \n",
    "histrograms:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12,5), dpi=80)\n",
    "bins = 15\n",
    "\n",
    "ax = fig.add_subplot(121)\n",
    "ax.hist(X[:,0], bins=bins)\n",
    "ax.set_xlabel(r'$x_1$', fontsize=20)\n",
    "ax.set_ylabel(r'$p$', fontsize=20)\n",
    "\n",
    "ax2 = fig.add_subplot(122)\n",
    "ax2.hist(X[:,1], bins=bins)\n",
    "ax2.set_xlabel(r'$x_2$', fontsize=20)\n",
    "ax2.set_ylabel(r'$p$', fontsize=20)\n",
    "\n",
    "plt.draw()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Since the sample we use is only two dimensional, we can even visualize them together:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(111, projection='3d')\n",
    "hist, xedges, yedges = np.histogram2d(X[:,0], X[:,1], bins=10, range=[[X_lim[0], X_lim[1]], [Y_lim[0], Y_lim[1]]])\n",
    "\n",
    "xpos, ypos = np.meshgrid(xedges[:-1], yedges[:-1])\n",
    "xpos = xpos.flatten('F')\n",
    "ypos = ypos.flatten('F')\n",
    "zpos = np.zeros_like(xpos)\n",
    "\n",
    "dx = 0.5 * np.ones_like(zpos)\n",
    "dy = dx.copy()\n",
    "dz = hist.flatten()\n",
    "\n",
    "ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color='cyan', zsort='average')\n",
    "ax.view_init(45, 45) # 90,90 = topdown\n",
    "\n",
    "plt.draw()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Considering the plots of the individual distributions and the joint distribution, it is not far-fetched to assume a bivariate normal distribution.\n",
    "\n",
    "Using the mean and the covariance matrix of the sample, it is possible to estimate the underlying gaussian probability density function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "\n",
    "M=60\n",
    "A = np.linspace(X_lim[0],X_lim[1], M)\n",
    "B = np.linspace(Y_lim[0],Y_lim[1], M)\n",
    "A_, B_ = np.meshgrid(A, B)\n",
    "xy = np.column_stack([A_.flat, B_.flat])\n",
    "\n",
    "# density values at the grid points\n",
    "Z = mvn.pdf(xy, mu, cov).reshape(A_.shape)\n",
    "\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(111, projection='3d')\n",
    "ax.set_xlabel(r'$x_1$', fontsize=20)\n",
    "ax.set_ylabel(r'$x_2$', fontsize=20)\n",
    "ax.set_zlabel(r'$p$', fontsize=20)\n",
    "ax.plot_surface(A_,B_,Z,cmap=cm.coolwarm)\n",
    "ax.view_init(45, 45) # 90,90 = topdown\n",
    "plt.draw()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "A way to visualize both, the drawn sample and the estimated bivariate gaussian is using a 2D-scatter-plot together with a contour-plot. In a contour-plot all data points on a line with the same colour have the same value in the third dimension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "# arbitrary contour levels\n",
    "max = 0.12\n",
    "contour_level = [max * 1/3, max * 2/3, max]\n",
    "fig = plt.figure(figsize=(7,7))\n",
    "ax = fig.add_subplot(111)\n",
    "\n",
    "ax.contour(A, B, Z, levels = contour_level)\n",
    "ax.scatter(X[...,0], X[...,1], c='b', marker='x', label=\"data\")\n",
    "ax.set_xlim(X_lim[0],X_lim[1])\n",
    "ax.set_ylim(Y_lim[0],Y_lim[1])\n",
    "ax.set_xlabel('$x_1$')\n",
    "ax.set_ylabel('$x_2$')\n",
    "plt.draw()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise - Joint Normality"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Task:**\n",
    "\n",
    "If $x_1, \\dots, x_p$ are all normally distributed, does this always imply that they are jointly normally distributed, i.e. does the pair $(x_1, \\dots, x_p)$ have a multivariate normal distribution?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "        <tr>\n",
    "        <td>\n",
    "            <a name=\"BIS07\"></a>[BIS07]\n",
    "        </td>\n",
    "        <td>\n",
    "            Christopher M. Bishop, Pattern recognition and machine learning, 5th Edition. Springer 2007, ISBN 9780387310732.\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"GOO16\"></a>[GOO16]\n",
    "        </td>\n",
    "        <td>\n",
    "            Goodfellow, Ian, et al. Deep learning. Vol. 1. Cambridge: MIT press, 2016.\n",
    "        </td>\n",
    "    </tr>\n",
    "        <tr>\n",
    "        <td>\n",
    "            <a name=\"MAT18\"></a>[MAT18]\n",
    "        </td>\n",
    "        <td>\n",
    "            mathematicalmonk. (2018, September 30). (ML 15.3) Logistic regression (binary) - intuition. And following in the playlist Machine Learning. Retrieved from https://www.youtube.com/watch?v=-Z2a_mzl9LM&list=PLD0F06AA0D2E8FFBA&t=740s&index=110\n",
    "        </td>\n",
    "    </tr>\n",
    "        </tr>\n",
    "        <tr>\n",
    "        <td>\n",
    "            <a name=\"KHA18\"></a>[KHA18]\n",
    "        </td>\n",
    "        <td>\n",
    "            Khan Academy. (2018, October 05). Deep definition of the normal distribution. And following in the playlist. Retrieved from https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/more-on-normal-distributions/v/introduction-to-the-normal-distribution\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "Exercise - Multivariate Gaussian <br/>\n",
    "by Christian Herta, Klaus Strohmenger<br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Christian Herta, Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
