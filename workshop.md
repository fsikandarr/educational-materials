# Workshop

We are happy to announce, that on **23th** of **Mai** **2019**, the *deep.TEACHING* team will offer a **free workshop**.


![ai-workshop](./media/klaus/ai-workshop.png)

## Facts
**Topic:** Machine Learning in Practice  
**Date:** 23th of May 2019
**Time:** 10:00 - 17:00  
**Address:** [HTW Berlin / CBMI - Ostendstraße 25, 12459 Berlin, Haus 1A](https://www.openstreetmap.org/search?query=Ostendstra%C3%9Fe%2025%2C%2012459%20Berlin#map=19/52.45724/13.53443)  
**Language:** German  
**Application Deadline:** 3rd of May 2019  

**Contact:** [Klaus.Strohmenger@HTW-Berlin.de](mailto:Klaus.Strohmenger@HTW-Berlin.de)

**Hardware:** bring a notebook capable for semi heavy calculations. A mobile i5 gen. 2xxx and upwards, 8 GByte of RAM should be sufficient.    
**Software:** your setup should either be prepared to run [docker files (recommended)](https://www.docker.com/get-started) or natively have the [listed packages](#faq) installed.

## Download Exercises and Solution

The exercises for the workshop can be downloaded [here](https://gitlab.com/deep.TEACHING/educational-materials/raw/master/workshop20190523.zip)

## Program

*Subject to change*

<table>
    <tr>
        <td>
            <b>10:00</b>
        </td>
        <td>
            <b>Welcoming and Presentation of the deep.TEACHING Project</b>
        </td>
    </tr>
    <tr>
        <td>
            <b>10:30</b>
        </td>
        <td>
            <b>
            Lectures
            <i>
            <ul>
              <li>What is Deep Learning?</li>
              <li>Deep Learning Libraries and Frameworks</li>
            </ul>
            </i>
            Exercises (Subject to change)
            <i>
            <ul>
              <li>Introduction into PyTorch</li>
            </ul
            </i>
            </b>
        </td>
    </tr>
    <tr>
        <td>
            <b>13:00</b>
        </td>
        <td>
            <b>Lunch Break</b>
        </td>
    </tr>
    <tr>
        <td>
            <b>14:00</b>
        </td>
        <td>
            <b>
            Excercises (Subject to change)
            <i>
            <ul>
              <li>Linear Regression (predicting float values)</li>
              <li>Logistic Regression (simple classifier)</li>
              <li>Fully Connected Neural Network (classifier)</li>
            </ul>
            </i>
            </b>
        </td>
    </tr>
    <tr>
        <td>
            <b>17:00</b>
        </td>
        <td>
            <b>Farewell</b>
        </td>
    </tr>
</table>

#### What You Get
* A mixture of interesting lectures and practical experience

#### What You Give
* Your time, ideas and dedication

## FAQ

#### How do I sign up?
Workshop participant slots are limited. If you are interested write an e-mail to [Klaus.Strohmenger@HTW-Berlin.de](mailto:Klaus.Strohmenger@HTW-Berlin.de).

#### Who is the targeted audience?
The course is aimed towards beginners in machine learning and those who want to get in touch with it. Though you should have basic experience in **(scientific) python**.

#### I do not want to use docker. Which software packages are required to natively run the notebooks?

**Note:** This is a provisional list and still subject to change. Please revisit this site one week before the workshop.

* Python3.6 or greater
* Jupyter Lab or Jupyter Notebook
* And the following Python packages:
  * numpy
  * matplotlib
  * pytorch
  * torchvision
  * pandas
  * scikit-learn
  * sklearn
  * scipy

#### How do I check if my setup is working correctly?
You can test if you have the right packages installed by downloading executing the following [Jupyter Notebook file](https://gitlab.com/deep.TEACHING/educational-materials/blob/master/notebooks/uncategorized/package_test_workshop_2019_05.ipynb). If no exception is thrown, everything should be fine, though we recommend to also prepare your notebook with docker just in case.

Further, installation instructions for the notebooks can be found [here](/#how-to-use-deepteaching-notebooks).

#### What can I do to prepare myself before taking part in the workshop?
For better learning experience we suggest refreshing your Python and numpy skills with [this](https://gitlab.com/deep.TEACHING/educational-materials/blob/master/notebooks/machine-learning-fundamentals/exercise-python-numpy-basics.ipynb) Jupyter Notebook exercise.
